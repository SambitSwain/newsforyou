const { ApolloServer, PubSub } = require('apollo-server');
const mongoose = require('mongoose');

const typeDefs = require('./graphql/typeDefs');
const resolvers = require('./graphql/resolvers');
const { MONGODB } = require('./config');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => ({ req, PubSub })
});

mongoose.connect(MONGODB, { useNewUrlParser: true })
    .then(() => {
        console.log('*** MongoDb Connected ***')
        return server.listen({ port: 4000 });
    }).then((res) => {
        console.log(`*** server is listining at ${res.port} ***`)
    })