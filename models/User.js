const { model, Schema } = require('mongoose');

const userSchema = new Schema({
    userName: String,
    email: String,
    password: String
})

userSchema.set('collection', 'users');

module.exports = model('User', userSchema);