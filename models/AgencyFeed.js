const { model, Schema } = require('mongoose');

const agencyFeedSchema = new Schema({
    url: String,
    category:
    {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    agency:
    {
        type: Schema.Types.ObjectId,
        ref: 'Agency'
    }
})

agencyFeedSchema.set('collection', 'agency_feed');

module.exports = model('AgencyFeed', agencyFeedSchema);