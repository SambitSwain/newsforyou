const { model, Schema } = require('mongoose');

const categorySchema = new Schema({
    title: String
})

categorySchema.set('collection', 'category');

module.exports = model('Category', categorySchema);