const { model, Schema } = require('mongoose');

const newsSchema = new Schema({
    title: String,
    description: String,
    pubDate: String,
    link: String,
    click_count: Schema.Types.Number,
    category:
    {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    agency:
    {
        type: Schema.Types.ObjectId,
        ref: 'Agency'
    }
})

newsSchema.set('collection', 'news');

module.exports = model('News', newsSchema);