const { model, Schema } = require('mongoose');

const agencySchema = new Schema({
    name: String,
    logo: String
})

agencySchema.set('collection', 'agency');

module.exports = model('Agency', agencySchema);