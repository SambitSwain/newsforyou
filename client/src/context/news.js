import React, { useReducer, createContext } from 'react';

const initialState = {
    news: {},
    publishedList: []
};

const NewsContext = createContext({
    news: {},
    publishedList: [],
    newNewsAdded: (newsData) => { },
    newNewsClick: (newsData) => { }
});

function newsReducer(state, action) {
    switch (action.type) {
        case 'RESETNEWS':
            state.news[action.payload.category] = []
            return state;
        case 'ADDNEWS':
            if (!state.news[action.payload.category]) {
                state.news[action.payload.category] = [];
            }
            state.news[action.payload.category].push(action.payload);
            return state;
        case 'PUBLISHED':
            const str = action.payload.agency + "_" + action.payload.category + "_" + action.payload.title;
            if (state.publishedList.indexOf(str) === -1) {
                state.publishedList.push(str)
            }
            return state;
        case 'CLICKED':
            // clicked
            return state;
        default:
            return state;
    }
}

function NewsProvider(props) {
    const [state, dispatch] = useReducer(newsReducer, initialState);

    function addNews(newsData) {
        dispatch({
            type: 'ADDNEWS',
            payload: newsData
        });
    }

    function resetNews(newsData) {
        dispatch({
            type: 'RESETNEWS',
            payload: newsData
        });
    }

    function published(newsData) {
        dispatch({
            type: 'PUBLISHED',
            payload: newsData
        });
    }

    function newsClicked(data) {
        dispatch({
            type: 'CLICKED',
            payload: data
        });
    }

    return (
        <NewsContext.Provider
            value={{
                news: state.news,
                publishedList: state.publishedList,
                addNews,
                resetNews,
                published,
                newsClicked
            }}
            {...props}
        />
    );
}

export { NewsContext, NewsProvider };