import React, { useState } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Dimmer, Modal, Loader, Table, Button, Container, Label, Image } from 'semantic-ui-react';

const ClickReport = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [newsList, setNewsList] = useState([]);
  const client = useApolloClient();

  const close = () => setOpen(false)

  const getClickReport = () => {
    setIsLoading(true);
    setOpen(true)
    client.query({
      query: GET_NEWS,
      variables: {
        category: null
      },
      options: {
        useUnifiedTopology: true
      }
    }).then(({ data }) => {
      setIsLoading(false)
      if (data) {
        setNewsList(data.getNews)
      }
    }).catch(error => {
      setIsLoading(false)
      console.log(error);
      alert(error)
    });
  }

  return (
    <Container>
      <Button onClick={getClickReport}>Get Click Report</Button>
      {
        isLoading ?
          <Dimmer active>
            <Loader content='Loading' />
          </Dimmer> :
          <div>
            <Modal dimmer open={open} onClose={close}>
              <Modal.Header>Click Report</Modal.Header>
              <Modal.Content>
                {(newsList.length > 0) ?
                  <Table celled>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Agency</Table.HeaderCell>
                        <Table.HeaderCell>Title</Table.HeaderCell>
                        <Table.HeaderCell>Clicks</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {newsList.map((item, index) => (
                        <Table.Row key={item.id}>
                          <Table.Cell style={{ display: "inline-flex" }}>
                            <Image src={item.agency.logo} size='mini' ></Image>
                            <Label>{item.agency.name}</Label>
                          </Table.Cell>
                          <Table.Cell>{item.title} </Table.Cell>
                          <Table.Cell>{item.click_count} </Table.Cell>
                        </Table.Row>
                      ))}
                    </Table.Body>
                  </Table>
                  : <p>No Clicks Yet!</p>
                }
              </Modal.Content>
              <Modal.Actions>
                <Button color='black' onClick={close}>
                  Close
                </Button>
              </Modal.Actions>
            </Modal>
          </div>
      }
    </Container>
  )
}

const GET_NEWS = gql`
  query getNews($category: ID) {
    getNews(category: $category) {
      id
      title
      pubDate
      click_count
      agency{
        name
        logo
      }
    }
  }
`;

export default ClickReport;