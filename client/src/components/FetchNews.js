import React, { useState, useEffect, useContext } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Dimmer, Form, Loader, Container } from 'semantic-ui-react';

import LinkPreview from './LinkPreview';
import { NewsContext } from '../context/news'

const FetchNews = ({ data: { category, agency } }) => {
    const { publishedList } = useContext(NewsContext);
    const [isLoading, setIsLoading] = useState(false);
    const [newsList, setNewsList] = useState([]);
    const client = useApolloClient();

    useEffect(() => {
        if (category !== "" && agency !== "") {
            setIsLoading(true)
            client.query({
                query: FETCH_NEWS,
                variables: {
                    category,
                    agency
                },
                options: {
                    useUnifiedTopology: true
                }
            }).then(({ data }) => {
                setIsLoading(false)
                if (data) {
                    const newsArr = data.fetchNews.map(({ __typename, ...rest }) => {
                        const alreadyPublished = (publishedList.indexOf(agency + "_" + category + "_" + rest.title) !== -1) ? true : false
                        return {
                            ...rest,
                            category,
                            agency,
                            alreadyPublished,
                            click_count: 0
                        }
                    });

                    setNewsList(newsArr);
                }
            }).catch(error => {
                setIsLoading(false)
                console.log(error);
                alert(error)
            });
        }
    }, [category, agency]);

    return (
        <Container>
            <Form.Group widths='equal' className='fetchHeader'>
                <h3>News Feed</h3>
            </Form.Group>
            {
                isLoading ?
                    <Dimmer active>
                        <Loader content='Loading' />
                    </Dimmer> :
                    <div>
                        {newsList.map((item, index) => (
                            <LinkPreview
                                key={index}
                                data={item}
                                category={category}
                                agency={agency}
                                showPublish={true}>
                            </LinkPreview>
                        ))}
                    </div>
            }
        </Container>
    )
}

const FETCH_NEWS = gql`
  query fetchNews($category: ID!, $agency: ID!) {
    fetchNews(category: $category, agency: $agency) {
        title
        link
        pubDate
        description
    }
  }
`;

export default FetchNews;