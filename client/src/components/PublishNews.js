import React, { useState, useEffect, useContext } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Button, Dimmer, Loader, Message } from 'semantic-ui-react';

import { NewsContext } from '../context/news'

const PublishNews = ({ newsInput }) => {
  const { published } = useContext(NewsContext);
  const [isLoading, setIsLoading] = useState(false);
  const [isPublished, setIsPublished] = useState(false);
  const client = useApolloClient();

  const publish = () => {
    setIsLoading(true);
    delete newsInput.alreadyPublished;

    client.mutate({
      mutation: PUBLISH_NEWS,
      variables: {
        newsInput
      }
    }).then(({ data }) => {
      setIsLoading(false);
      setIsPublished(true);
      published(newsInput)
    }).catch(error => {
      setIsLoading(false)
      setIsPublished(false)
      alert(error)
    });
  }

  useEffect(() => {
    setIsPublished(newsInput.alreadyPublished)
  }, [newsInput]);

  return (
    <>
      {
        isLoading ?
          <Dimmer active>
            <Loader content='Loading' />
          </Dimmer> :
          <div>
            {
              (isPublished) ? <Message
                success
                visible
                header=''
                content="Published Successfully!"
              /> :
                <Button onClick={publish} color='green'>Publish</Button>
            }
          </div>
      }
    </>
  )
}

const PUBLISH_NEWS = gql`
  mutation publishNews($newsInput: NewsInput!) {
    publishNews(newsInput: $newsInput) {
      success
      error
    }
  }
`;

export default PublishNews;