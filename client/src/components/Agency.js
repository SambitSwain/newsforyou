import React, { useState, useEffect } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Dimmer, Form, Loader } from 'semantic-ui-react';

const Agency = ({ handleChangeCallBack }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [agencyList, setAgencyList] = useState([]);
    const client = useApolloClient();

    useEffect(() => {
        client.query({
            query: GET_AGENCIES,
            variables: {
                str: ""
            },
            options: {
                useUnifiedTopology: true
            }
        }).then(({ loading, data }) => {
            setIsLoading(false)
            if (data) {
                const agencyArr = data.getAgency.map(item => {
                    return {
                        key: item.id,
                        value: item.id,
                        text: item.name,
                    }
                });
                setAgencyList(agencyArr);
            }
        }).catch(error => {
            setIsLoading(false)
            console.log(error);
            alert(error)
        });
    }, [])

    const handleChange = (e, { name, value }) => {
        handleChangeCallBack(name, value)
    }

    return (
        <>
            {
                isLoading ?
                    <Dimmer active>
                        <Loader content='Loading' />
                    </Dimmer> :
                    <Form.Select
                        name='agency'
                        options={agencyList}
                        placeholder='Select Agency'
                        onChange={handleChange}
                    />
            }
        </>
    )
}

const GET_AGENCIES = gql`
  query getAgency($str: String!) {
    getAgency(str: $str) {
      id
      name
      logo
    }
  }
`;

export default Agency;