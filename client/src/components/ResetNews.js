import React, { useState } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Button, Dimmer, Loader, Message } from 'semantic-ui-react';

const ResetNews = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [result, setResult] = useState("");
  const client = useApolloClient();

  const reset = () => {
    setResult("")
    setIsLoading(true);
    client.mutate({
      mutation: RESET_NEWS,
      variables: {
        str: ""
      }
    }).then(({ data }) => {
      setIsLoading(false);
      setResult("DB Cleared Successfully!")
    }).catch(error => {
      console.log(error)
      setIsLoading(false);
      setResult("Error in Cleared DB!")
      alert(error)
    });
  }

  return (
    <>
      {
        isLoading ?
          <Dimmer active>
            <Loader content='Loading' />
          </Dimmer> :
          <div>
            <Button negative onClick={reset}>Reset DB</Button>
            {
              result !== "" &&
              <Message
                success
                visible
                header=''
                content={result}
              />
            }
          </div>
      }

    </>
  )
}

const RESET_NEWS = gql`
  mutation resetNews($str: String!) {
    resetNews(str: $str) {
      success
      error
    }
  }
`;

export default ResetNews;