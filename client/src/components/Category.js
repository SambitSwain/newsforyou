import React, { useState, useEffect } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Dimmer, Form, Loader } from 'semantic-ui-react';

const Category = ({ handleChangeCallBack }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [categoryList, setCategoryList] = useState([]);
    const client = useApolloClient();

    useEffect(() => {
        client.query({
            query: GET_CATEGORIES,
            variables: {
                str: ""
            },
            options: {
                useUnifiedTopology: true
            }
        }).then(({ data }) => {
            setIsLoading(false)
            if (data) {
                const categoriesArr = data.getCategory.map(item => {
                    return {
                        key: item.id,
                        value: item.id,
                        text: item.title,
                    }
                });
                setCategoryList(categoriesArr);
            }
        }).catch(error => {
            setIsLoading(false)
            console.log(error);
            alert(error)
        });
    }, [])

    const handleChange = (e, { name, value }) => {
        handleChangeCallBack(name, value)
    }

    return (
        <>
            {
                isLoading ?
                    <Dimmer active>
                        <Loader content='Loading' />
                    </Dimmer>
                    :
                    <Form.Select
                        fluid
                        name='category'
                        options={categoryList}
                        placeholder='Select Category'
                        onChange={handleChange}
                    />
            }

        </>
    );
}

const GET_CATEGORIES = gql`
  query getCategory($str: String!) {
    getCategory(str: $str) {
      id
      title
    }
  }
`;

export default Category;
