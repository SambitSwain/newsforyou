import React, { useState, useEffect, useContext } from 'react';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';
import { Dimmer, Header, Loader, Form, Divider } from 'semantic-ui-react';

import LinkPreview from './LinkPreview';
import { NewsContext } from '../context/news'

const GetNews = ({ category }) => {
    const { news, addNews, resetNews } = useContext(NewsContext);
    const [isLoading, setIsLoading] = useState(false);
    const [newsList, setNewsList] = useState([]);
    const client = useApolloClient();

    useEffect(() => {
        if (category !== "") {
            if (news[category] && news[category].length > 0) {
                setNewsList(news[category])
            } else {
                setIsLoading(true)
                client.query({
                    query: GET_NEWS,
                    variables: {
                        category
                    },
                    options: {
                        useUnifiedTopology: true
                    }
                }).then(({ data }) => {
                    setIsLoading(false)
                    if (data) {
                        resetNews({ category })
                        data.getNews.forEach(function (item) {
                            addNews({ ...item, category })
                        });
                        setNewsList(data.getNews)
                    }
                }).catch(error => {
                    setIsLoading(false)
                    console.log(error);
                    alert(error)
                });
            }
        }
    }, [category, news]);

    useEffect(() => {
        const observer = client.subscribe({
            query: NEW_NEWS_SUB,
            variables: {
                str: ""
            },
        })

        const subscription = observer.subscribe(({ data }) => {
            if (data) {
                const newsData = {
                    ...data.newNewsSub,
                    category: data.newNewsSub.category.id
                }
                setIsLoading(true);
                addNews(newsData);
                setNewsList(newsList => {
                    let isPresent = false
                    newsList.forEach(function (item) {
                        if (item.id === newsData.id) {
                            isPresent = true;
                            return;
                        }
                    })
                    if (!isPresent) {
                        newsList.push(newsData)
                    }
                    return newsList;
                });
                setIsLoading(false);
            }
        })
        return () => subscription.unsubscribe()
    }, []);

    return (
        <Form>
            <Header>News</Header>
            <Divider></Divider>
            {
                isLoading ?
                    <Dimmer active>
                        <Loader content='Loading' />
                    </Dimmer> :
                    <div>
                        {newsList.map((item, index) => (
                            <LinkPreview
                                key={Math.random()}
                                data={item}
                                showPublish={false}>
                            </LinkPreview>
                        ))}
                    </div>
            }
        </Form>
    )
}

const GET_NEWS = gql`
  query getNews($category: ID) {
    getNews(category: $category) {
        id
        title
        link
        pubDate
        description
    }
  }
`;

const NEW_NEWS_SUB = gql`
  subscription newNewsSub($str: String!) {
    newNewsSub(str: $str) {
      id
      title
      description
      pubDate
      link
      category{
        id
      }
    }
  }
`;

export default GetNews;