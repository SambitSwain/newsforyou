import React from 'react';
import { Segment, Label } from 'semantic-ui-react';
import ReactHtmlParser from 'react-html-parser';
import { useApolloClient } from "@apollo/react-hooks";
import gql from 'graphql-tag';

import PublishNews from './PublishNews';

const LinkPreview = ({ data, data: { id, link, title, description, pubDate }, category, agency, showPublish }) => {
    const client = useApolloClient();

    const linkClicked = () => {
        client.mutate({
            mutation: UPDATE_CLICK,
            variables: {
                id
            }
        }).then(({ data }) => {
        }).catch(error => {
            console.log(error)
        });
    }

    return (
        <Segment color='teal'>
            <a href={link} target='_blank' rel="noopener noreferrer" onClick={linkClicked}>
                <h3>{title}</h3></a>
            <Label size='mini'>{pubDate}</Label>
            <div>{ReactHtmlParser(description)}</div>
            <br />
            {showPublish && <PublishNews newsInput={data} category={category} agency={agency}></PublishNews>}
        </Segment>
    )
}

const UPDATE_CLICK = gql`
  mutation updateClickCount($id: ID!) {
    updateClickCount(id: $id) {
      success
      error
    }
  }
`;

export default LinkPreview;