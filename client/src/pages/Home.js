import React, { useState } from 'react';

import { Form, Container, Segment, Header, Divider } from 'semantic-ui-react';

import Category from '../components/Category';
import Agency from '../components/Agency';
import FetchNews from '../components/FetchNews';
import ResetNews from '../components/ResetNews';
import ClickReport from '../components/ClickReport';
import { NewsProvider } from '../context/news'

function Home() {
  const [variables, setVariables] = useState({
    category: "",
    agency: ""
  })

  const handleChange = (name, value) => {
    setVariables({
      ...variables,
      [name]: value
    })
  }

  return (
    <Container>
      <NewsProvider>
        <Form>
          <Segment>
            <Form.Group widths='equal' style={{ "justifyContent": "space-between" }}>
              <Segment>
                <Header as='h3' dividing>
                  Fetch News Feed
              </Header>
                <Category handleChangeCallBack={handleChange}></Category>
                <Agency handleChangeCallBack={handleChange}></Agency>
              </Segment>
              <ResetNews></ResetNews>
            </Form.Group>
            <Divider />
            <ClickReport></ClickReport>
            <Divider />
            <FetchNews data={variables}></FetchNews>
          </Segment>
        </Form>
      </NewsProvider>
    </Container>
  );
}

export default Home;
