import React, { useContext, useState } from 'react';

import { Grid, Header, Segment, Button, Message, Form, List} from 'semantic-ui-react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';

import { AuthContext } from '../context/auth';
import { useForm } from '../util/hooks';

function Login(props) {
    const context = useContext(AuthContext);
    const [errors, setErrors] = useState({});

    const { onChange, onSubmit, values } = useForm(loginUserCallback, {
        email: '',
        password: ''
    });

    const [loginUser, { loading }] = useMutation(LOGIN_USER, {
        update(
            _,
            {
                data: { login: userData }
            }
        ) {
            context.login(userData);
            props.history.push('/');
        },
        onError(err) {
            setErrors(err.graphQLErrors[0].extensions.exception.errors);
        },
        variables: values
    });

    function loginUserCallback() {
        loginUser();
    }

    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Log-in to your account
                </Header>
                <Form size='large' onSubmit={onSubmit} noValidate className={loading ? 'loading' : ''}>
                    <Segment stacked>
                        <Form.Input
                            fluid
                            iconPosition='left'
                            placeholder="E-mail address"
                            name="email"
                            type="text"
                            icon="mail"
                            value={values.email}
                            error={errors.email ? true : false}
                            onChange={onChange}
                        />
                        <Form.Input
                            fluid
                            iconPosition='left'
                            placeholder="Password"
                            name="password"
                            type="password"
                            icon="lock"
                            value={values.password}
                            error={errors.password ? true : false}
                            onChange={onChange}
                        />
                        <Button color='teal' fluid size='large'>
                            Login
                        </Button>
                    </Segment>
                </Form>
                {Object.keys(errors).length > 0 && (
                   <Message negative>
                        <ul className="list">
                            {Object.values(errors).map((value) => (
                                <List key={value}>{value}</List>
                            ))}
                        </ul>
                    </Message>
                )}
            </Grid.Column>
        </Grid>
    )
}

const LOGIN_USER = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
      userName
      token
    }
  }
`;

export default Login