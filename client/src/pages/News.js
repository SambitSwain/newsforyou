import React, { useState } from 'react';
import { Grid, Segment } from 'semantic-ui-react';

import { NewsProvider } from '../context/news'
import Category from '../components/Category';
import GetNews from '../components/GetNews';

function News() {
  const [selectedCategory, setSelectedCategory] = useState("");

  const handleChange = (name, value) => {
    setSelectedCategory(value)
  }

  return (
    <Segment>
      <NewsProvider>
        <Grid columns={2} relaxed='very'>
          <Grid.Column width={3} color='grey'>
            <div>
              <Category handleChangeCallBack={handleChange}></Category>
            </div>
          </Grid.Column>
          <Grid.Column>
            <div>
              <GetNews category={selectedCategory}></GetNews>
            </div>
          </Grid.Column>
        </Grid>
      </NewsProvider>
    </Segment>
  );
}

export default News;
