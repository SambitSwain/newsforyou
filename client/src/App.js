import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { ApolloProvider } from 'react-apollo';

import './App.css';
import 'semantic-ui-css/semantic.min.css';

import { AuthProvider } from './context/auth';


import AuthRoute from './util/AuthRoute';
import Home from './pages/Home';
import Login from './pages/Login';
import News from './pages/News';

const httpLink = new HttpLink({
  uri: 'http://localhost:4000/graphql'
})

const wsLink = new WebSocketLink({
  uri: 'ws://localhost:4000/graphql',
  options: {
    reconnect: true
  }
})

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLink
)

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  connectToDevTools: true
})

function App() {
  return (
    <ApolloProvider client={client}>
      <AuthProvider>
          <Router>
            <AuthRoute exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/news" component={News} />
          </Router>
      </AuthProvider>
    </ApolloProvider>
  );
}

export default App;
