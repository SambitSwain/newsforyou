const { gql } = require('apollo-server');

module.exports = gql`
    type Category {
        id: ID!
        title: String!
    }
    type Agency {
        id: ID!
        name: String!
        logo: String!
    }
    type User {
        id: ID!
        email: String!
        userName: String!
        token: String!
    }
    type News{
        id:ID!
        title: String!
        description: String!
        pubDate: String!
        link: String!
        click_count: Int
        category: Category!
        agency: Agency!
    }
    input NewsInput {
        title: String!
        description: String!
        pubDate: String!
        link: String!
        click_count: Int
        category: ID!
        agency: ID!
    }
    type Feed{
        title: String!
        description: String!
        pubDate: String!
        link: String!
    }
    type Result{
        success: String
        error: String
    }
    type Query {
        getCategory(str:String!): [Category]
        getAgency(str:String!): [Agency]
        getUsers(str:String!): [User]
        fetchNews(category:ID!, agency:ID!): [Feed]
        getNews(category:ID): [News]
    }
    type Mutation {
        login(email: String!, password: String!): User!
        publishNews(newsInput: NewsInput!): Result
        resetNews(str: String!): Result
        updateClickCount(id: ID!): Result
    }
    type Subscription {
        newNewsSub(str:String): News
    }
`;