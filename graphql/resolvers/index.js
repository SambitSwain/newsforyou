const userResolvers = require('./users');
const categoryResolvers = require('./categories');
const agencyResolvers = require('./agencies');
const newsResolvers = require('./news');

module.exports = {
  Query: {
    ...categoryResolvers.Query,
    ...agencyResolvers.Query,
    ...userResolvers.Query,
    ...newsResolvers.Query
  },
  Mutation: {
    ...userResolvers.Mutation,
    ...newsResolvers.Mutation
  },
  Subscription: {
    ...newsResolvers.Subscription
  }
};