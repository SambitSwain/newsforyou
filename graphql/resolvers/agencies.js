const Agency = require('../../models/Agency');

module.exports = {
    Query: {
        async getAgency(_, { str }) {
            const agencies = await Agency.find();
            if (agencies) {
                const agenciesArr = agencies.map(item => {
                    return {
                        ...item._doc,
                        id: item._id,
                    }
                });
                return agenciesArr;
            } else {
                return [];
            }
        }
    }
};
