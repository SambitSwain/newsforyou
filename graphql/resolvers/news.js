const { PubSub, withFilter } = require('apollo-server');
let Parser = require('rss-parser');

const News = require('../../models/News');
const AgencyFeed = require('../../models/AgencyFeed');

const pubsub = new PubSub();
const NEW_NEWS_SUB = 'NEW_NEWS_SUB';

module.exports = {
    Subscription: {
        newNewsSub: {
            subscribe: withFilter(
                () => pubsub.asyncIterator([NEW_NEWS_SUB]),
                (payload, args) => {
                    return true;
                }
            )
        }
    },
    Query: {
        async fetchNews(_, { category, agency }) {
            const news = await AgencyFeed.findOne({ category, agency });
            if (news) {
                let parser = new Parser();
                let feed = await parser.parseURL(news.url);
                const newsArr = [];
                feed.items.forEach(item => {
                    newsArr.push({
                        title: item.title,
                        description: item.content,
                        pubDate: item.pubDate,
                        link: item.link
                    })
                });
                return newsArr;
            } else {
                return [];
            }
        },
        async getNews(_, { category }) {
            let news;
            if (category) {
                news = await News.find({ category }).sort({ pubDate: 1 })
            } else {
                news = await News.find({ click_count: { $gt: 0 } }).sort({ click_count: -1 })
                    .populate({ path: 'agency', select: 'id name logo' })
            }

            if (news) {
                const newsList = news.map(item => {
                    return {
                        ...item._doc,
                        id: item._id
                    }
                });
                return newsList;
            } else {
                return [];
            }
        }
    },
    Mutation: {
        async publishNews(_, { newsInput }) {
            News.findOneAndUpdate(newsInput, newsInput, { upsert: true, new: true }, function (err, doc) {
                if (err) {
                    return {
                        error: "true"
                    };
                } else {
                    const newNewsSub = {
                        id: doc._id,
                        title: doc.title,
                        description: doc.description,
                        pubDate: doc.pubDate,
                        link: doc.link,
                        category: {
                            id: doc.category
                        }
                    }

                    pubsub.publish(NEW_NEWS_SUB, { newNewsSub });
                    return {
                        success: "true"
                    };
                }
            });
        },
        async resetNews() {
            await News.remove(function (err, doc) {
                if (err) {
                    return {
                        error: "true"
                    };
                } else {
                    return {
                        success: "true"
                    };
                }
            });
        },
        async updateClickCount(_, { id }) {
            News.findOneAndUpdate({ _id: id }, { $inc: { click_count: 1 } }, { new: true }, function (err, doc) {
                if (err) {
                    return {
                        error: "true"
                    };
                } else {
                    return {
                        success: "true"
                    };
                }
            });
        }
    }
};
