const Category = require('../../models/Category');

module.exports = {
    Query: {
        async getCategory(_, { str }) {
            const categories = await Category.find();
            if (categories) {
                const categoriesArr = categories.map(item => {
                    return {
                        ...item._doc,
                        id: item._id,
                    }
                });
                return categoriesArr;
            } else {
                return [];
            }
        }
    }
};
